.. _Categorized layer to class fraction layer:

*****************************************
Categorized layer to class fraction layer
*****************************************

Aggregates a (single-band) categorized layer into a (multiband) class fraction raster, by resampling into the given grid. Output band order and naming are given by the renderer categories.

**Parameters**


:guilabel:`Categorized layer` [layer]
    A categorized layer with categories to be aggregated into fractions.


:guilabel:`Grid` [raster]
    The target grid.

**Outputs**


:guilabel:`Output class fraction layer` [rasterDestination]
    Output raster file destination.

