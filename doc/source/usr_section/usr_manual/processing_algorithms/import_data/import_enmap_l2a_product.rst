.. _Import EnMAP L2A Product:

************************
Import EnMAP L2A Product
************************

Return VRT dataset with spectral information (wavelength and FWHM) and data gains/offsets.

**Parameters**


:guilabel:`METADATA.XML` [file]
    Metadata file associated with L2A product.

**Outputs**


:guilabel:`Output VRT` [rasterDestination]
    Specify output path.

    Default: *EnMAP_L2A_SPECTRAL.vrt*

