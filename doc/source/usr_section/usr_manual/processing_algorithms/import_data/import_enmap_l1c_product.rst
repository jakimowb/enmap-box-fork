.. _Import EnMAP L1C Product:

************************
Import EnMAP L1C Product
************************

Return VRT dataset with spectral information (wavelength and FWHM) and data gains/offsets.

**Parameters**


:guilabel:`METADATA.XML` [file]
    Metadata file associated with L1C product.

**Outputs**


:guilabel:`Output VRT` [rasterDestination]
    Specify output path.

    Default: *EnMAP_L1C_SPECTRAL.vrt*

