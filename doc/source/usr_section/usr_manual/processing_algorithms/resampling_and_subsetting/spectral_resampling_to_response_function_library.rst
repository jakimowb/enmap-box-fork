.. _Spectral Resampling to Response Function Library:

************************************************
Spectral Resampling to Response Function Library
************************************************

Spectrally resample a raster using a response function library.

**Parameters**


:guilabel:`Raster` [raster]
    Select raster to be resampled.


:guilabel:`Response Function Library.` [vector]
    Library with bandwise response function profiles.


:guilabel:`Resampling Algorithm` [enum]
    undocumented parameter

    Default: *1*

**Outputs**


:guilabel:`Output Raster` [rasterDestination]
    Specify output path for raster.

