.. _Spectral Resampling to Raster:

*****************************
Spectral Resampling to Raster
*****************************

Spectrally resample a raster to a raster.

**Parameters**


:guilabel:`Raster` [raster]
    Select raster to be resampled.


:guilabel:`Target Raster` [raster]
    Raster with defined wavelength and (optional) fwhm


:guilabel:`Resampling Algorithm` [enum]
    undocumented parameter

    Default: *1*

**Outputs**


:guilabel:`Output Raster` [rasterDestination]
    Specify output path for raster.

