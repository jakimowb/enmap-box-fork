.. _Create Change Map:

*****************
Create Change Map
*****************

Create a change map from two classifications.

**Parameters**


:guilabel:`Classification 1` [raster]
    Select a classification layer.


:guilabel:`Classification 2` [raster]
    Select a classification layer.


:guilabel:`Show Change Matrix` [boolean]
    

**Outputs**


:guilabel:`Output Raster` [rasterDestination]
    Specify output path.

