.. _Classifier performance report:

*****************************
Classifier performance report
*****************************

Evaluates classifier performance.

**Parameters**


:guilabel:`Classifier` [file]
    Classifier pickle file.


:guilabel:`Test dataset` [file]
    Test dataset pickle file used for assessing the classifier performance.


:guilabel:`Number of cross-validation folds` [number]
    The number of folds used for assessing cross-validation performance. If not specified (default), simple test performance is assessed.

**Outputs**


:guilabel:`Output report` [fileDestination]
    Output report file destination.

