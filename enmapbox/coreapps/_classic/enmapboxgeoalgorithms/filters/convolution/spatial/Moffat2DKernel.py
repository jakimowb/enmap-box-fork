from astropy.convolution import Moffat2DKernel

kernel = Moffat2DKernel(gamma=3, alpha=2)
