from enmapboxgeoalgorithms.estimators import parseFolder

def parseOtherFilter():
    return parseFolder(package='enmapboxgeoalgorithms.filters.other')
