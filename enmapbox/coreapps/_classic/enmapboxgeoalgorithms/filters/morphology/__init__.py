from enmapboxgeoalgorithms.estimators import parseFolder

def parseMorphology():
    return parseFolder(package='enmapboxgeoalgorithms.filters.morphology')
